package tokenauth

type BadTokenAuth struct {
	AuthToken string  `json:"auth_token"`
	Data      BadData `json:"data"`
	Error     string  `json:"error"`
	Message   string  `json:"message"`
	RequestID string  `json:"request_id"`
	Status    string  `json:"status"`
}

type BadData struct {
	Message string `json:"message"`
}

type GoodTokenAuth struct {
	AuthToken string   `json:"auth_token"`
	RequestID string   `json:"request_id"`
	Revision  string   `json:"revision"`
	Status    string   `json:"status"`
	Data      GoodData `json:"data"`
}

type GoodData struct {
	AccountID   string        `json:"account_id"`
	AccountName string        `json:"account_name"`
	Apps        []interface{} `json:"apps"`
	ID          string        `json:"id"`
	IsReseller  bool          `json:"is_reseller"`
	Language    string        `json:"language"`
	Method      string        `json:"method"`
	OwnerID     string        `json:"owner_id"`
	ResellerID  string        `json:"reseller_id"`
}
