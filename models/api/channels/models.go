package channels

type Channel struct {
	AuthToken string        `json:"auth_token"`
	Data      []ChannelData `json:"data"`
	RequestID string        `json:"request_id"`
	Revision  string        `json:"revision"`
	Status    string        `json:"status"`
}

type ChannelData struct {
	Answered        bool   `json:"answered"`
	AuthorizingID   string `json:"authorizing_id"`
	AuthorizingType string `json:"authorizing_type"`
	Destination     string `json:"destination"`
	Direction       string `json:"direction"`
	OtherLeg        string `json:"other_leg"`
	OwnerID         string `json:"owner_id"`
	PresenceID      string `json:"presence_id"`
	Timestamp       int64  `json:"timestamp"`
	Username        string `json:"username"`
	UUID            string `json:"uuid"`
}
