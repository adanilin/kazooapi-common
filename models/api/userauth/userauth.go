package userauth

import (
	"gitlab.com/adanilin/kazooapi-common/v3/models/api"
)

type UserAuth struct {
	api.CommonPart
	Data UserAuthData `json:"data"`
	Apps UserAuthApps `json:"apps"`
}

type UserAuthApps struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Label string `json:"label"`
}

type UserAuthData struct {
	OwnerID     string `json:"owner_id"`
	AccountID   string `json:"account_id"`
	ResellerID  string `json:"reseller_id"`
	IsReseller  bool   `json:"is_reseller"`
	AccountName string `json:"account_name"`
	Language    string `json:"language"`
}
