package callflow

import "gitlab.com/adanilin/kazooapi-common/v3/models/api"

type Model struct {
	api.CommonPart
	Data Data `json:"data"`
}
type FlowData struct {
	ID      string `json:"id"`
	Timeout int    `json:"timeout"`
}
type Flow struct {
	Data   *FlowData `json:"data"`
	Module string    `json:"module"`
}
type Data struct {
	ID      string   `json:"id,omitempty"`
	Flow    *Flow    `json:"flow"`
	Name    string   `json:"name"`
	Numbers []string `json:"numbers"`
	OwnerID string   `json:"owner_id"`
	Type    string   `json:"type"`
}

func CreateDefaultCallflow(userId string, callflowName string, phoneNumbers []string) Model {
	r := Model{}
	r.Data = Data{
		Flow: &Flow{
			Data: &FlowData{
				ID:      userId,
				Timeout: 30,
			},
			Module: "user",
		},
		Name:    callflowName,
		Numbers: phoneNumbers,
		OwnerID: userId,
		Type:    "user",
	}
	return r
}
