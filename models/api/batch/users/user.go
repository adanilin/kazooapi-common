package users

import "gitlab.com/adanilin/kazooapi-common/v3/models/api"

type Response struct {
	Data []Data `json:"data"`
	api.CommonPart
}

type Onnet struct {
	Enabled bool `json:"enabled"`
}
type Offnet struct {
	Enabled bool `json:"enabled"`
}
type Inbound struct {
	Onnet  Onnet  `json:"onnet"`
	Offnet Offnet `json:"offnet"`
}
type Outbound struct {
	Onnet  Onnet  `json:"onnet"`
	Offnet Offnet `json:"offnet"`
}
type CallRecording struct {
	Inbound  Inbound  `json:"inbound"`
	Outbound Outbound `json:"outbound"`
}
type Data struct {
	ID            string        `json:"id"`
	Features      []interface{} `json:"features"`
	Username      string        `json:"username"`
	Email         string        `json:"email"`
	FirstName     string        `json:"first_name"`
	LastName      string        `json:"last_name"`
	PrivLevel     string        `json:"priv_level"`
	Flags         []interface{} `json:"flags"`
	PresenceID    string        `json:"presence_id,omitempty"`
	CallRecording CallRecording `json:"call_recording,omitempty"`
	FailedReason  string        `json:"-"`
}
