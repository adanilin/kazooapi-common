package vmboxes

import "gitlab.com/adanilin/kazooapi-common/v3/models/api"

type Response struct {
	api.CommonPart
	Data []Data `json:"data"`
}
type Data struct {
	ID       string        `json:"id"`
	Name     string        `json:"name"`
	Mailbox  string        `json:"mailbox"`
	OwnerID  string        `json:"owner_id"`
	Messages int           `json:"messages"`
	Flags    []interface{} `json:"flags"`
}
