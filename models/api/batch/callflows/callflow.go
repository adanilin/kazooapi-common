package callflows

import "gitlab.com/adanilin/kazooapi-common/v3/models/api"

type Response struct {
	api.CommonPart
	Data []Data `json:"data"`
}
type ExtraInfo struct {
}
type Data struct {
	ID         string        `json:"id"`
	Name       string        `json:"name"`
	Username   string        `json:"username"`
	MacAddress string        `json:"mac_address"`
	OwnerID    string        `json:"owner_id"`
	Enabled    bool          `json:"enabled"`
	DeviceType string        `json:"device_type"`
	Flags      []interface{} `json:"flags"`
	ExtraInfo  ExtraInfo     `json:"extra_info"`
}
