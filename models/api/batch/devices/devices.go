package devices

import "gitlab.com/adanilin/kazooapi-common/v3/models/api"

type Response struct {
	api.CommonPart
	Data []Data `json:"data"`
}
type Data struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Type        string   `json:"type"`
	Numbers     []string `json:"numbers"`
	Patterns    any      `json:"patterns"`
	Featurecode any      `json:"featurecode"`
	OwnerID     string   `json:"owner_id"`
	Modules     []string `json:"modules"`
	Flags       any      `json:"flags"`
}
