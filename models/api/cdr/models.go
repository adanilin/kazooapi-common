package cdr

import (
	"gitlab.com/adanilin/kazooapi-common/v3/models/api"
	"time"
)

type Cdr struct {
	NextStartKey string `json:"next_start_key"`
	PageSize     string `json:"page_size"`
	StartKey     string `json:"start_key"`
	Data         []Data `json:"data"`
	api.CommonPart
}

type Data struct {
	ID              string        `json:"id"`
	CallID          string        `json:"call_id"`
	CallerIDNumber  string        `json:"caller_id_number"`
	CallerIDName    string        `json:"caller_id_name"`
	CalleeIDNumber  string        `json:"callee_id_number"`
	CalleeIDName    string        `json:"callee_id_name"`
	DurationSeconds int           `json:"duration_seconds"`
	BillingSeconds  int           `json:"billing_seconds"`
	Timestamp       string        `json:"timestamp"`
	HangupCause     string        `json:"hangup_cause"`
	OtherLegCallID  string        `json:"other_leg_call_id"`
	OwnerID         string        `json:"owner_id"`
	To              string        `json:"to"`
	From            string        `json:"from"`
	Direction       string        `json:"direction"`
	Request         string        `json:"request"`
	AuthorizingID   string        `json:"authorizing_id"`
	Cost            string        `json:"cost"`
	DialedNumber    string        `json:"dialed_number"`
	CallingFrom     string        `json:"calling_from"`
	Datetime        string        `json:"datetime"`
	UnixTimestamp   string        `json:"unix_timestamp"`
	Rfc1036         string        `json:"rfc_1036"`
	Iso8601         string        `json:"iso_8601"`
	Iso8601Combined time.Time     `json:"iso_8601_combined"`
	CallType        string        `json:"call_type"`
	Rate            string        `json:"rate"`
	RateName        string        `json:"rate_name"`
	BridgeID        string        `json:"bridge_id"`
	RecordingURL    string        `json:"recording_url"`
	MediaRecordings []interface{} `json:"media_recordings"`
	MediaServer     string        `json:"media_server"`
	CallPriority    string        `json:"call_priority"`
	InteractionID   string        `json:"interaction_id"`
	OomaCsType      string        `json:"ooma_cs_type"`
	OomaCsFrom      string        `json:"ooma_cs_from"`
	OomaCsTo        string        `json:"ooma_cs_to"`
	OomaCsFwd       string        `json:"ooma_cs_fwd"`
	OomaUserFwd     string        `json:"ooma_user_fwd"`
	OomaCfType      string        `json:"ooma_cf_type"`
	CallForward     string        `json:"call_forward"`
	OomaFromOrig    string        `json:"ooma_from_orig"`
}
