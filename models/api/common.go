package api

import "time"

type CommonPart struct {
	NextStartKey string      `json:"next_start_key"`
	PageSize     int         `json:"page_size"`
	Revision     string      `json:"revision"`
	Timestamp    time.Time   `json:"timestamp"`
	Version      string      `json:"version"`
	Node         string      `json:"node"`
	RequestID    string      `json:"request_id"`
	Status       string      `json:"status"`
	AuthToken    string      `json:"auth_token"`
	Data         interface{} `json:"data"`
}
