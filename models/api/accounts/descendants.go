package accounts

import (
	"encoding/json"
	"github.com/go-resty/resty/v2"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api"
)

type Descendants struct {
	Data []Descendant `json:"data"`
	api.CommonPart
}

type Descendant struct {
	ID    string        `json:"id"`
	Name  string        `json:"name"`
	Realm string        `json:"realm"`
	Tree  []string      `json:"tree"`
	Flags []interface{} `json:"flags"`
}

func NewDescendantsFromResponse(response *resty.Response) Descendants {
	ca := Descendants{}
	json.Unmarshal(response.Body(), &ca)
	return ca
}
