package accounts

import (
	"encoding/json"
	"github.com/go-resty/resty/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api"
)

type ApiKeyResponse struct {
	api.CommonPart
	Data struct {
		ApiKey string `json:"api_key"`
	} `json:"data"`
}

func NewApiKeyResponseFromResponse(response *resty.Response) ApiKeyResponse {
	akr := ApiKeyResponse{}
	err := json.Unmarshal(response.Body(), &akr)
	if err != nil {
		log.Fatal(err)
	}
	return akr
}
