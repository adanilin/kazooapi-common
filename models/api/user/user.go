package user

import "fmt"

type Model struct {
	Data Data `json:"data"`
}
type Internal struct {
	Number string `json:"number"`
	Name   string `json:"name"`
}
type External struct {
	Number string `json:"number"`
}
type CallerID struct {
	Internal Internal `json:"internal"`
	External External `json:"external"`
}
type CallForward struct {
	Enabled      bool `json:"enabled"`
	KeepCallerID bool `json:"keep_caller_id"`
	Substitute   bool `json:"substitute"`
}
type DD struct {
	Prefix string `json:"prefix"`
	Name   string `json:"name"`
}
type DialPlans struct {
	DialPlan map[string]DD `json:"dial_plan"`
}
type Onnet struct {
	Enabled bool `json:"enabled"`
}
type Offnet struct {
	Enabled bool `json:"enabled"`
}
type Inbound struct {
	Onnet  Onnet  `json:"onnet"`
	Offnet Offnet `json:"offnet"`
}
type Outbound struct {
	Onnet  Onnet  `json:"onnet"`
	Offnet Offnet `json:"offnet"`
}
type CallRecording struct {
	Inbound  Inbound  `json:"inbound"`
	Outbound Outbound `json:"outbound"`
}
type Data struct {
	ID               string        `json:"id,omitempty"`
	FirstName        string        `json:"first_name"`
	LastName         string        `json:"last_name"`
	Email            string        `json:"email"`
	Password         string        `json:"password"`
	Username         string        `json:"username"`
	CallerID         CallerID      `json:"caller_id"`
	CallForward      CallForward   `json:"call_forward"`
	VMToEmailEnabled bool          `json:"vm_to_email_enabled"`
	AllowUserMobile  bool          `json:"allow_user_mobile"`
	DialPlans        DialPlans     `json:"dial_plan"`
	PresenceID       string        `json:"presence_id"`
	CallRecording    CallRecording `json:"call_recording"`
}

func CreateDefaultUser(firstName string, lastName string, extensionNumberAsUsername string, externalNumber string) Model {
	rr := Model{Data: Data{
		FirstName: firstName,
		LastName:  lastName,
		Email:     fmt.Sprintf("%v.%v@q.com", firstName, lastName),
		Password:  "12345678",
		Username:  extensionNumberAsUsername,
		CallerID: CallerID{
			Internal: Internal{
				Number: extensionNumberAsUsername,
				Name:   fmt.Sprintf("%v %v", firstName, lastName),
			},
			External: External{Number: externalNumber},
		},
		CallForward: CallForward{
			Enabled:      false,
			KeepCallerID: true,
			Substitute:   true,
		},
		VMToEmailEnabled: false,
		AllowUserMobile:  true,
		DialPlans: DialPlans{map[string]DD{`^(\\d{7})$`: {
			Prefix: "+1818",
			Name:   "LD_818",
		}}},
		PresenceID: extensionNumberAsUsername,
		CallRecording: CallRecording{
			Inbound: Inbound{
				Onnet:  Onnet{Enabled: false},
				Offnet: Offnet{Enabled: false},
			},
			Outbound: Outbound{Onnet: Onnet{Enabled: false}, Offnet: Offnet{Enabled: false}},
		},
	}}
	return rr
}
