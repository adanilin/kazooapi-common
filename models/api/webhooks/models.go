package webhooks

import (
	"gitlab.com/adanilin/kazooapi-common/v3/models/api"
)

type Schemas struct {
	Data struct {
		Schema      string `json:"$schema"`
		Description string `json:"description"`
		Properties  struct {
			CustomData struct {
				Additionalproperties struct {
					Type string `json:"type"`
				} `json:"additionalProperties"`
				Description string `json:"description"`
				Type        string `json:"type"`
			} `json:"custom_data"`
			Enabled struct {
				Default     bool   `json:"default"`
				Description string `json:"description"`
				Type        string `json:"type"`
			} `json:"enabled"`
			Format struct {
				Default      string   `json:"default"`
				Description  string   `json:"description"`
				Enum         []string `json:"enum"`
				SupportLevel string   `json:"support_level"`
				Type         string   `json:"type"`
			} `json:"format"`
			Hook struct {
				Description  string   `json:"description"`
				SupportLevel string   `json:"support_level"`
				Type         string   `json:"type"`
				Enum         []string `json:"enum"`
			} `json:"hook"`
			HTTPVerb struct {
				Default      string   `json:"default"`
				Description  string   `json:"description"`
				Enum         []string `json:"enum"`
				SupportLevel string   `json:"support_level"`
				Type         string   `json:"type"`
			} `json:"http_verb"`
			IncludeInternalLegs struct {
				Default     bool   `json:"default"`
				Description string `json:"description"`
				Type        string `json:"type"`
			} `json:"include_internal_legs"`
			IncludeSubaccounts struct {
				Description  string `json:"description"`
				SupportLevel string `json:"support_level"`
				Type         string `json:"type"`
			} `json:"include_subaccounts"`
			Name struct {
				Description  string `json:"description"`
				SupportLevel string `json:"support_level"`
				Type         string `json:"type"`
			} `json:"name"`
			Retries struct {
				Default      int    `json:"default"`
				Description  string `json:"description"`
				Maximum      int    `json:"maximum"`
				Minimum      int    `json:"minimum"`
				SupportLevel string `json:"support_level"`
				Type         string `json:"type"`
			} `json:"retries"`
			URI struct {
				Description     string `json:"description"`
				Format          string `json:"format"`
				KazooValidation bool   `json:"kazoo-validation"`
				SupportLevel    string `json:"support_level"`
				Type            string `json:"type"`
			} `json:"uri"`
		} `json:"properties"`
		Required []string `json:"required"`
		Type     string   `json:"type"`
		ID       string   `json:"id"`
	} `json:"data"`
}

type Create struct {
	Data CreateData `json:"data"`
}

type CreateData struct {
	Name               string `json:"name"`
	Uri                string `json:"uri"`
	HttpVerb           string `json:"http_verb"`
	Hook               string `json:"hook"`
	Retries            int    `json:"retries"`
	Format             string `json:"format"`
	IncludeSubaccounts bool   `json:"include_subaccounts"`
}

type ListOfWebhooks struct {
	PageSize int    `json:"page_size"`
	StartKey string `json:"start_key"`
	api.CommonPart
	Data []WebhookData `json:"data"`
}

type WebhookData struct {
	ID      string `json:"id"`
	Hook    string `json:"hook"`
	Name    string `json:"name"`
	URI     string `json:"uri"`
	Enabled bool   `json:"enabled"`
}

type IncomingWebhook struct {
	AccountID             string `json:"account_id"`
	AuthorizingID         string `json:"authorizing_id"`
	AuthorizingType       string `json:"authorizing_type"`
	CallDirection         string `json:"call_direction"`
	CallForwarded         bool   `json:"call_forwarded"`
	CallID                string `json:"call_id"`
	CalleeIDName          string `json:"callee_id_name"`
	CalleeIDNumber        string `json:"callee_id_number"`
	CallerIDName          string `json:"caller_id_name"`
	CallerIDNumber        string `json:"caller_id_number"`
	HangupCode            string `json:"hangup_code"`
	HangupCause           string `json:"hangup_cause"`
	DurationSeconds       int    `json:"duration_seconds"`
	RingingSeconds        int    `json:"ringing_seconds"`
	CustomApplicationVars struct {
	} `json:"custom_application_vars"`
	CustomChannelVars struct {
	} `json:"custom_channel_vars"`
	EmergencyResourceUsed bool   `json:"emergency_resource_used"`
	From                  string `json:"from"`
	Inception             string `json:"inception"`
	IsInternalLeg         bool   `json:"is_internal_leg"`
	LocalResourceID       string `json:"local_resource_id"`
	LocalResourceUsed     bool   `json:"local_resource_used"`
	OtherLegCallID        string `json:"other_leg_call_id"`
	OwnerID               string `json:"owner_id"`
	Request               string `json:"request"`
	ResellerID            string `json:"reseller_id"`
	//Timestamp             int `json:"timestamp"`
	To        string `json:"to"`
	HookEvent string `json:"hook_event"`
}
