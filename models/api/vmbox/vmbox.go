package vmbox

type Model struct {
	Data Data `json:"data"`
}
type Media struct {
}
type Data struct {
	ID                   string   `json:"id,omitempty"`
	Name                 string   `json:"name"`
	RequirePin           bool     `json:"require_pin"`
	IsSetup              bool     `json:"is_setup"`
	Pin                  string   `json:"pin"`
	Mailbox              string   `json:"mailbox"`
	Timezone             string   `json:"timezone"`
	CheckIfOwner         bool     `json:"check_if_owner"`
	DeleteAfterNotify    bool     `json:"delete_after_notify"`
	NotConfigurable      bool     `json:"not_configurable"`
	NotifyEmailAddresses []string `json:"notify_email_addresses"`
	SaveAfterNotify      bool     `json:"save_after_notify"`
	SkipGreeting         bool     `json:"skip_greeting"`
	SkipInstructions     bool     `json:"skip_instructions"`
	OwnerID              string   `json:"owner_id"`
	Media                Media    `json:"media"`
}

func CreateDefaultVmbox(userId string, name string, number string) Model {
	model := Model{Data: Data{
		Name:                 name,
		RequirePin:           true,
		IsSetup:              false,
		Pin:                  "1234",
		Mailbox:              number,
		Timezone:             "Europe/Moscow",
		CheckIfOwner:         true,
		DeleteAfterNotify:    false,
		NotConfigurable:      false,
		NotifyEmailAddresses: []string{},
		SaveAfterNotify:      false,
		SkipGreeting:         false,
		SkipInstructions:     false,
		OwnerID:              userId,
		Media:                Media{},
	}}
	return model
}
