package device

type Response struct {
	Data Data `json:"data"`
}
type Audio struct {
	Codecs []string `json:"codecs"`
}
type Media struct {
	BypassMedia bool   `json:"bypass_media"`
	Audio       *Audio `json:"audio"`
}

type CustomSipHeaders struct {
	Out map[string]string `json:"out,omitempty"`
	In  map[string]string `json:"in,omitempty"`
}
type Sip struct {
	Username         string            `json:"username"`
	Password         string            `json:"password"`
	CustomSipHeaders *CustomSipHeaders `json:"custom_sip_headers,omitempty"`
}
type Push struct {
	TokenProxy string `json:"Token-Proxy"`
}
type CallForward struct {
	KeepCallerID    bool `json:"keep_caller_id"`
	RequireKeypress bool `json:"require_keypress"`
}
type Data struct {
	ID                 string       `json:"id,omitempty"`
	DeviceType         string       `json:"device_type"`
	Name               string       `json:"name"`
	Media              Media        `json:"media"`
	Sip                *Sip         `json:"sip"`
	OwnerID            string       `json:"owner_id"`
	Enabled            bool         `json:"enabled"`
	Nine11TcAccepted   bool         `json:"911_tc_accepted"`
	InboundTcAccepted  bool         `json:"inbound_tc_accepted"`
	OutboundTcAccepted bool         `json:"outbound_tc_accepted"`
	InboundEnabled     bool         `json:"inbound_enabled"`
	OutboundEnabled    bool         `json:"outbound_enabled"`
	Push               *Push        `json:"push,omitempty"`
	CallForward        *CallForward `json:"call_forward"`
}

func CreateDefaultDevice(userId string, name string, sipUsername string, device_type string, password string, bypassMedia bool) Response {
	r := Response{}
	r.Data = Data{
		Sip: &Sip{
			Username: sipUsername,
			Password: password,
		},
		OwnerID: userId,
		Name:    name,
		Media: Media{
			BypassMedia: bypassMedia,
			Audio:       &Audio{Codecs: []string{"OPUS", "PCMU", "PCMA"}},
		},
		Enabled:    true,
		DeviceType: device_type,
		CallForward: &CallForward{
			KeepCallerID: true,
		},
	}
	if device_type == "mobile" {
		r.Data.Sip.CustomSipHeaders.Out = map[string]string{
			"X-ooma-911-ecrc":      "1",
			"X-ooma-cellular-mode": "false",
		}
		r.Data.Sip.CustomSipHeaders.In = map[string]string{
			"X-ooma-push":          "false",
			"X-ooma-cellular-mode": "false",
		}
		r.Data.Push = &Push{}
		r.Data.Push.TokenProxy = "sip:pushsmb.ooma.internal."
	}
	return r
}
