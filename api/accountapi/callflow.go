package accountapi

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/adanilin/kazooapi-common/v3/api/requesthelper"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/callflow"
)

func (acc *Account) CreateCallflow(c callflow.Model) (callflow.Model, string, error) {
	response, err := acc.AccountRequest("PUT", "/callflows", func(r *resty.Request) {
		r.SetBody(c)
	})
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return callflow.Model{}, responseStr, err
	}
	r := callflow.Model{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, responseStr, err
}

func (acc *Account) DeleteCallflow(callflowId string) (callflow.Model, string, error) {
	response, err := acc.AccountRequest("DELETE", fmt.Sprintf("/callflows/%v", callflowId))
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return callflow.Model{}, responseStr, err
	}
	r := callflow.Model{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, responseStr, err
}
