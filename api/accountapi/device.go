package accountapi

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/adanilin/kazooapi-common/v3/api/requesthelper"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/device"
	"net/http"
)

func (acc *Account) GetDevice(deviceId string) (device.Data, string, error) {
	response, err := acc.AccountRequest("GET", fmt.Sprintf("/devices/%v", deviceId))
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return device.Data{}, responseStr, err
	}
	r := device.Response{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r.Data, responseStr, err
}

func (acc *Account) DeleteDevice(deviceId string) (device.Data, string, error) {
	response, err := acc.AccountRequest("DELETE", fmt.Sprintf("/devices/%v", deviceId))
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return device.Data{}, responseStr, err
	}
	r := device.Response{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r.Data, responseStr, err
}

func (acc *Account) CreateDevice(d device.Response) (device.Response, string, error) {
	response, err := acc.AccountRequest("PUT", "/devices", func(r *resty.Request) {
		r.SetBody(d)
	})
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return device.Response{}, responseStr, err
	}
	r := device.Response{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, responseStr, err
}

func (acc *Account) PatchDevice(deviceId string, mediaByPass bool) error {
	bypass := "true"
	if !mediaByPass {
		bypass = "false"
	}
	mf := func(r *resty.Request) {
		r.SetBody(fmt.Sprintf(`{"data": {"media": { "bypass_media": %v } } }`, bypass))
	}
	response, err := acc.AccountRequest("PATCH", fmt.Sprintf("/devices/%v", deviceId), mf)
	if err != nil {
		return err
	}
	if response.StatusCode() != http.StatusOK {
		return errors.New(response.String())
	}
	r := device.Response{}
	err = json.Unmarshal(response.Body(), &r)
	if err != nil {
		return err
	}
	if r.Data.Media.BypassMedia != mediaByPass {
		return errors.New(response.String())
	}
	err = requesthelper.CheckStatusCode(response, err)
	return nil
}
