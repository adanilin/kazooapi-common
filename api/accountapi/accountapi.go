package accountapi

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"github.com/go-logr/logr"
	"github.com/go-resty/resty/v2"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/adanilin/kazooapi-common/v3/api/apihelper"
	"gitlab.com/adanilin/kazooapi-common/v3/api/requesthelper"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/accounts"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/batch/callflows"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/batch/devices"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/batch/users"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/batch/vmboxes"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/tokenauth"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/userauth"
	"golang.org/x/sync/errgroup"
	"math/rand"
	"net/http"
	"time"
)

type ApiSettings struct {
	pageSize string
}

func (as *ApiSettings) SetPageSize(ps string) {
	as.pageSize = ps
}

func (acc *Account) SetRequestTimeout(duration time.Duration) {
	acc.restyClient.SetTimeout(duration)
}

type Account struct {
	restyClient  *resty.Client
	kazooBaseUrl string
	AccountName  string
	AccountUser  string
	AuthDigest   string
	AccountId    string
	AuthToken    string
	Proxies      string
	Users        []users.Data
	Vmboxes      []vmboxes.Data
	Devices      []devices.Data
	Callflows    []callflows.Data
	ApiKey       string
	Logger       logr.Logger
	ApiSettings  ApiSettings
}

func (acc *Account) FillAccountData() *Account {
	vs := acc.ApiSettings.pageSize
	logger := acc.Logger.WithName("FillAccountData")
	if acc.Users == nil {
		acc.Users = []users.Data{}
	}
	logger.Info("Filling data in the account")
	wg := errgroup.Group{}
	cLogger := logger.WithName("Users")
	cLogger.Info("Fetching users in background")
	wg.Go(func() error {
		nextStartKey := ""
		for {
			response, err := acc.AccountRequest("GET", "/users", requesthelper.SetPageSize(vs), requesthelper.SetStartKey(nextStartKey))
			if err != nil {
				panic(err)
			}
			users := users.Response{}
			err = json.Unmarshal(response.Body(), &users)
			if err != nil {
				panic(err)
			}
			acc.Users = append(acc.Users, users.Data...)
			if users.NextStartKey != "" {
				nextStartKey = users.NextStartKey
				continue // TODO
			}
			break
		}
		return nil
	})
	cLogger = logger.WithName("Vmboxes")
	cLogger.Info("Fetching vmboxes in background")
	wg.Go(func() error {
		nextStartKey := ""
		for {
			response, err := acc.AccountRequest("GET", "/vmboxes", requesthelper.SetPageSize(vs), requesthelper.SetStartKey(nextStartKey))
			if err != nil {
				panic(err)
			}
			vmboxes := vmboxes.Response{}
			err = json.Unmarshal(response.Body(), &vmboxes)
			if err != nil {
				panic(err)
			}
			acc.Vmboxes = append(acc.Vmboxes, vmboxes.Data...)
			if vmboxes.NextStartKey != "" {
				nextStartKey = vmboxes.NextStartKey
				continue
			}
			break
		}
		return nil
	})
	cLogger = logger.WithName("Callflows")
	cLogger.Info("Fetching callflows in background")

	wg.Go(func() error {
		nextStartKey := ""
		for {

			response, err := acc.AccountRequest("GET", "/callflows", requesthelper.SetPageSize(vs), requesthelper.SetStartKey(nextStartKey))
			if err != nil {
				panic(err)
			}
			callflows := callflows.Response{}
			err = json.Unmarshal(response.Body(), &callflows)
			if err != nil {
				panic(err)
			}
			acc.Callflows = append(acc.Callflows, callflows.Data...)
			if callflows.NextStartKey != "" {
				nextStartKey = callflows.NextStartKey
				continue
			}
			break
		}
		return nil
	})
	cLogger = logger.WithName("Response")
	cLogger.Info("Fetching devices in background")
	wg.Go(func() error {
		nextStartKey := ""
		for {
			response, err := acc.AccountRequest("GET", "/devices", requesthelper.SetPageSize(vs), requesthelper.SetStartKey(nextStartKey))
			if err != nil {
				panic(err)
			}
			devices := devices.Response{}
			err = json.Unmarshal(response.Body(), &devices)
			if err != nil {
				panic(err)
			}
			acc.Devices = append(acc.Devices, devices.Data...)
			if devices.NextStartKey != "" {
				nextStartKey = devices.NextStartKey
				continue
			}
			break
		}
		return nil
	})
	wg.Wait()
	return acc
}

func (acc *Account) CheckTokenValidity() (bool, error) {
	acc.Logger.Info("Checking tokens")
	response, err := acc.RequestWithoutAccount("GET", "v2", "/token_auth", nil)
	if err != nil {
		return false, err
	}
	if response.StatusCode() == 401 {
		return false, nil
	}
	badTokenAuth := tokenauth.BadTokenAuth{}

	err = json.Unmarshal(response.Body(), &badTokenAuth)
	if err != nil {
		return false, err
	}
	if badTokenAuth.Error == "" {
		return true, nil
	}
	return false, nil
}

func (acc *Account) GetAllChildAccounts() []*Account {
	response, err := acc.AccountRequest("GET", "/descendants?paginate=false")
	if err != nil {
		log.Fatal(err)
	}
	children := accounts.NewDescendantsFromResponse(response)
	accountsSlice := make([]*Account, 0)
	for _, child := range children.Data {
		response, err = acc.AccountRequest("GET", fmt.Sprintf("/accounts/%v/api_key", child.ID))
		apiKeyResponse := accounts.NewApiKeyResponseFromResponse(response)
		if err != nil {
			acc.Logger.Error(err, "error")
			panic(err)
		}
		childAcc := NewAccountApiAuth(acc.kazooBaseUrl, child.Name, apiKeyResponse.Data.ApiKey)
		accountsSlice = append(accountsSlice, childAcc)
	}
	return accountsSlice
}

func NewAccountBasicAuth(kazooBaseUrl string, accountUser string, accountPassword string, accountName string) *Account {
	account := NewAccount()
	account.kazooBaseUrl = kazooBaseUrl
	account.AccountUser = accountUser
	account.AccountName = accountName
	str := fmt.Sprintf("%v:%v", account.AccountUser, accountPassword)
	md5sum := md5.Sum([]byte(str))

	account.AuthDigest = fmt.Sprintf("%x", md5sum)
	account.Logger = apihelper.GetLogger().WithValues("Account", account.AccountName)
	account.authenticate(false)
	return account
}

func NewAccountApiAuth(kazooBaseUrl string, accountUser string, apiKey string) *Account {
	account := NewAccount()
	account.kazooBaseUrl = kazooBaseUrl
	account.AccountUser = accountUser
	account.ApiKey = apiKey
	account.Logger = apihelper.GetLogger().WithValues("Account", account.AccountUser)
	account.authenticate(true)
	return account
}

func (acc *Account) RequestWithoutAccount(method string, version string, url string, mutateFuncs ...func(r *resty.Request)) (*resty.Response, error) {
	if mutateFuncs == nil {
		mutateFuncs = []func(r *resty.Request){}
	}
	mutateFuncs = append(mutateFuncs, func(r *resty.Request) {
		r.SetHeader("X-Auth-Token", acc.AuthToken)
	})
	constructedUrl := fmt.Sprintf("%v/%v%v", acc.kazooBaseUrl, version, url)
	return acc.sendRequest(method, constructedUrl, mutateFuncs...)
}

func (acc *Account) AccountRequest(method string, path string, mutateFuncs ...func(r *resty.Request)) (*resty.Response, error) {
	if mutateFuncs == nil {
		mutateFuncs = []func(r *resty.Request){}
	}
	mutateFuncs = append(mutateFuncs, func(r *resty.Request) {
		r.SetHeader("X-Auth-Token", acc.AuthToken)
	})
	constructedUrl := fmt.Sprintf("%v/%v/accounts/%v%v", acc.kazooBaseUrl, "v2", acc.AccountId, path)
	return acc.sendRequest(method, constructedUrl, mutateFuncs...)
}

func (acc *Account) authenticate(viaApi bool) (token string) {
	urlPath := ""
	data := ""
	if viaApi {
		urlPath = "/v2/api_auth"
		data = fmt.Sprintf(`{"data": {"api_key":"%v"}}`, acc.ApiKey)
	} else {
		urlPath = "/v2/user_auth"
		data = fmt.Sprintf(`{"data": {"credentials":"%v", "account_name":"%v", "method":"md5"}}`, acc.AuthDigest, acc.AccountName)
	}
	mutateFunc := func(r *resty.Request) {
		r.SetBody(data)
		r.SetResult(&userauth.UserAuth{})
	}
	response, err := acc.sendRequest("PUT", fmt.Sprintf("%v%v", acc.kazooBaseUrl, urlPath), mutateFunc)
	if err != nil {
		log.Fatal(err)
	}
	if response.StatusCode() == http.StatusServiceUnavailable {
		panic(response.String())
	}
	userAuth, ok := response.Result().(*userauth.UserAuth)
	if !ok {
		panic(err)
	}
	acc.AuthToken = userAuth.AuthToken
	acc.AccountId = userAuth.Data.AccountID
	acc.AccountName = userAuth.Data.AccountName
	return acc.AuthToken
}

func (acc *Account) sendRequest(method string, url string, mutateFuncs ...func(r *resty.Request)) (*resty.Response, error) {
	logger := acc.Logger.WithName("SendRequest")
	r := acc.restyClient.R()
	for _, f := range mutateFuncs {
		f(r)
	}
	if r.Header.Get("Accept") == "" {
		r.SetHeader("Accept", "application/json")
	}
	if r.Header.Get("Content-Type") == "" {
		r.SetHeader("Content-Type", "application/json")
	}
	for k, v := range r.QueryParam {
		logger = logger.WithValues(fmt.Sprintf("Query param '%v'", k), v)
	}
	rand.Seed(time.Now().UnixNano())
	id := uuid.New()
	logger = logger.WithValues("UUID", id, "URL", url, "Method", method)
	logger.V(1).Info(fmt.Sprintf("Sending a request"))
	start := time.Now()
	response, err := r.Execute(method, url)
	elapsed := time.Since(start)
	if err != nil {
		return nil, err
	}
	logger = logger.WithValues("StatusCode", response.StatusCode(), "Status", response.Status(), "Took", elapsed)
	logger.V(1).Info(fmt.Sprintf("Received a response"))
	return response, err
}

func NewAccount() *Account {
	account := &Account{}
	account.ApiSettings = ApiSettings{pageSize: "500"}
	account.restyClient = resty.New()
	account.SetRequestTimeout(time.Second * 60)
	return account
}
