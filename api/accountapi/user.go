package accountapi

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/adanilin/kazooapi-common/v3/api/requesthelper"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/user"
)

func (acc *Account) GetUser(userId string) (user.Model, error) {
	response, err := acc.AccountRequest("GET", fmt.Sprintf("/users/%v", userId))
	//responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return user.Model{}, err
	}
	r := user.Model{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, err
}

func (acc *Account) CreateUser(u user.Model) (user.Model, string, error) {
	response, err := acc.AccountRequest("PUT", "/users", func(r *resty.Request) {
		r.SetBody(u)
	})
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return user.Model{}, responseStr, err
	}
	r := user.Model{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, responseStr, err
}

func (acc *Account) DeleteUser(userId string) (user.Model, string, error) {
	response, err := acc.AccountRequest("DELETE", fmt.Sprintf("/users/%v", userId))
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return user.Model{}, responseStr, err
	}
	r := user.Model{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, responseStr, err
}
