package accountapi

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty/v2"
	"gitlab.com/adanilin/kazooapi-common/v3/api/requesthelper"
	"gitlab.com/adanilin/kazooapi-common/v3/models/api/vmbox"
)

func (acc *Account) CreateVmbox(vb vmbox.Model) (vmbox.Model, string, error) {
	response, err := acc.AccountRequest("PUT", "/vmboxes", func(r *resty.Request) {
		r.SetBody(vb)
	})
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return vmbox.Model{}, responseStr, err
	}
	r := vmbox.Model{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, responseStr, err
}

func (acc *Account) DeleteVmbox(vmboxId string) (vmbox.Model, string, error) {
	response, err := acc.AccountRequest("DELETE", fmt.Sprintf("/vmboxes/%v", vmboxId))
	responseStr, err := requesthelper.ReturnResponseInfo(response, err)
	if err != nil {
		return vmbox.Model{}, responseStr, err
	}
	r := vmbox.Model{}
	err = json.Unmarshal(response.Body(), &r)
	err = requesthelper.CheckStatusCode(response, err)
	return r, responseStr, err
}
