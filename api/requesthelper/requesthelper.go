package requesthelper

import (
	"errors"
	"github.com/go-resty/resty/v2"
	"net/http"
)

func SetPageSize(ps string) func(r *resty.Request) {
	return func(r *resty.Request) {
		r.SetQueryParam("page_size", ps)
	}
}

func SetStartKey(startKey string) func(r *resty.Request) {
	return func(r *resty.Request) {
		if startKey != "" {
			r.SetQueryParam("start_key", startKey)
		}
	}
}

func CheckStatusCode(response *resty.Response, err error) error {
	if err != nil {
		return err
	}
	if response.StatusCode() != http.StatusOK && response.StatusCode() != http.StatusCreated {
		return errors.New("status code of the response is not 200 or 201")
	}
	return nil
}

func ReturnResponseInfo(response *resty.Response, err error) (string, error) {
	if response == nil {
		return "", err
	}
	return response.String(), err
}
