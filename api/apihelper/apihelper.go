package apihelper

import (
	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var mainLogger logr.Logger

func init() {
	loggerCfg := zap.NewDevelopmentConfig()
	loggerCfg.Level = zap.NewAtomicLevelAt(zapcore.Level(0))
	zapLog, _ := loggerCfg.Build()
	mainLogger = zapr.NewLogger(zapLog)
}

func GetLogger() logr.Logger {
	return mainLogger
}
